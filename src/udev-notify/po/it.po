# Italian translation for udev-notify
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the udev-notify package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: udev-notify\n"
"Report-Msgid-Bugs-To: lfu.project@gmail.com\n"
"POT-Creation-Date: 2011-05-15 13:42+0300\n"
"PO-Revision-Date: 2011-05-25 16:24+0000\n"
"Last-Translator: Wormhole <Unknown>\n"
"Language-Team: Italian <it@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-08-15 05:09+0000\n"
"X-Generator: Launchpad (build 13674)\n"
"Language: it\n"

#: src/udev-notify.py:21
msgid "Display verbose output on STDOUT"
msgstr "Mostra l'output completo su l'uscita standard"

#: src/udev-notify.py:27
msgid "Cannot initialize libnotify"
msgstr "Non riesco ad avviare libnotify"

#: src/udev-notify.py:32
msgid "CD-ROM Drive"
msgstr "Unità CD-ROM"

#: src/udev-notify.py:41
msgid "Multimedia Player"
msgstr "Lettore Multimediale"

#: src/udev-notify.py:60 src/udev-notify.py:71 src/udev-notify.py:82
#: src/udev-notify.py:107 src/udev-notify.py:118
msgid "USB Storage Device"
msgstr "Periferica di massa USB"

#: src/udev-notify.py:93
msgid "SD/MMC Memory"
msgstr "Memoria SD/MMC"

#: src/udev-notify.py:100
msgid "Memory Stick"
msgstr "Memory Stick"

#: src/udev-notify.py:128
msgid "WiFi Device"
msgstr "Periferica WIfi"

#: src/udev-notify.py:136
msgid "WebCam / TV Tuner"
msgstr "WebCam / Sintonizzatore TV"

#: src/udev-notify.py:143
msgid "Mouse"
msgstr "Mouse"

#: src/udev-notify.py:152
msgid "Game Controller"
msgstr "Controller di Gioco"

#: src/udev-notify.py:161
msgid "Sound Card"
msgstr "Scheda audio"

#: src/udev-notify.py:170
msgid "USB Modem"
msgstr "Modem USB"

#: src/udev-notify.py:178
msgid "Modem"
msgstr "Modem"

#: src/udev-notify.py:186
msgid "PDA Device"
msgstr "Periferica PDA"

#: src/udev-notify.py:194
msgid "Keyboard"
msgstr "Tastiera"

#: src/udev-notify.py:203
msgid "Digital Camera"
msgstr "Macchina fotografica digitale"

#: src/udev-notify.py:211
msgid "Network Device"
msgstr "Periferica di Rete"

#: src/udev-notify.py:219
msgid "USB Network Device"
msgstr "Periferica di Rete USB"

#: src/udev-notify.py:227
msgid "USB Device"
msgstr "Periferica USB"

#: src/udev-notify.py:308
msgid "Device recognized"
msgstr "Periferica riconosciuta"

#: src/udev-notify.py:315
msgid "Device removed"
msgstr "Periferica rimossa"
