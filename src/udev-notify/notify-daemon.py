#!/usr/bin/env python

import sys, time
import notify2
import locale
import gettext
import re
from copy import deepcopy
from daemon import daemon
from gi.repository import Notify
from pyudev import Context, Monitor, MonitorObserver

DIR = '/usr/share/locale/'
APP = 'udev-notify'
locale.setlocale(locale.LC_ALL, '')
gettext.bindtextdomain(APP, DIR)
gettext.textdomain(APP)
_ = gettext.gettext

class NotifyDaemon(daemon):
    deviceTypes = [
            {
                'type': _('CD-ROM Drive'),
                'icon': 'media-optical-symbolic',
                'detection': {
                    'DEVTYPE': 'disk',
                    'ID_TYPE': 'cd',
                    'SUBSYSTEM': 'block' 
                }
            },
            {
                'type': _('Multimedia Player'), #this must be BEFORE generic storage and USB deviceTypes
                'icon': 'multimedia-player',
                'detection': {
                    'DEVTYPE':'usb_device',
                    'SUBSYSTEM': 'usb',
                    'ID_MEDIA_PLAYER': '*',
                }
            },
            {
                'type': _('Disk Partition'),
                'icon': 'drive-removable-media',
                'detection': {
                    'ID_FS_USAGE':'filesystem',
                    'ID_TYPE':'disk',
                    'SUBSYSTEM':'block',
                    'DEVTYPE':'partition'
                }
            },
            {
                'type': _('Disk Partition'),
                'icon': 'drive-removable-media',
                'detection': {
                    'SUBSYSTEM':'bdi',
                }
            },
            {
                'type': _('USB Modem'),
                'icon': 'modem',
                'detection': {
                    'ID_BUS':'usb',
                    'SUBSYSTEM': 'tty',
                }
            },
            {
                'type': _('Modem'),
                'icon': 'modem',
                'detection': {
                    'ID_USB_DRIVER': 'cdc_acm',
                    'SUBSYSTEM': 'tty'
                }
            },
            {
                'type': _('PDA Device'),
                'icon': 'pda',
                'detection': {
                    'ID_USB_DRIVER': 'ipaq',
                    'SUBSYSTEM': 'tty'
                }
            },    
            {
                'type': _('Keyboard'),
                'icon': 'gnome-dev-keyboard',
                'detection': {
                    'ID_CLASS': 'kbd',
                    'ID_TYPE': 'hid',
                    'SUBSYSTEM': 'input'
                }
            },
            {
                'type': _('Digital Camera'),
                'icon': 'camera-photo',
                'detection': {
                    'DEVTYPE':'usb_device',
                    'ID_GPHOTO2': '1',
                }
            },    
            {
                'type': _('Network Device'),
                'icon': 'network-wired',
                'detection': {
                    'ID_BUS':'pci',
                    'SUBSYSTEM': 'net',
                }
            },
            {
                'type': _('USB Network Device'),
                'icon': 'network-wired',
                'detection': {
                    'ID_BUS':'usb',
                    'SUBSYSTEM': 'net',
                }
            },
            {
                'type': _('USB Device'),
                'icon': 'computer-apple-ipad-symbolic',
                'detection': {
                    'DEVTYPE':'usb_device',
                    'SUBSYSTEM': 'usb',
                }
            },
            {
                'type': _('Mouse'),
                'icon': 'input-mouse-symbolic',
                'detection': {
                    'ID_INPUT_MOUSE': '1',
                    'ID_TYPE': 'hid',
                    'SUBSYSTEM': 'input'
                    }
                }
            ]
    def cleanstr_cb(self, m):
        return chr(int(eval('0'+m.group(1))))

    def cleanstr(self, text):
        text = re.sub(r'\\(x([a-f0-9]{2}))', self.cleanstr_cb, text).strip()
        return text
    def detect_device(self,device):
        for i in device:
            print (i,device[i])
        print ("-------------")
        # Iterate the devices stored internally
        for deviceType in self.deviceTypes:
            deviceType['name'] = ''
            detected = False  
            keys = deviceType['detection'].keys()
            for key in keys:
                # if the key exists in the monitored device
                if key in device:
                    # Have one match
                    if device[key] == deviceType['detection'][key] or deviceType['detection'][key] == '*':
                        detected = True
                    else:
                        detected = False
                        break
                else:
                    detected = False
                    break
                    # continue
            if detected:
                result = deepcopy(deviceType)
                result['action'] = str(device.action)
                
                if 'UDISKS_PRESENTATION_ICON_NAME' in device:
                    result['icon'] = device['UDISKS_PRESENTATION_ICON_NAME']
                else:
                    result['icon'] = result['icon']
                
                device_name = []
                
                if 'ID_VENDOR_FROM_DATABASE' in device:
                    device_name.append(device['ID_VENDOR_FROM_DATABASE'].strip())
                
                if 'ID_MODEL_FROM_DATABASE' in device:
                    if device_name:
                        device_name.append(device['ID_MODEL_FROM_DATABASE'].strip())
                
                if len(device_name) == 0 and 'ID_MODEL_ENC' in device:
                    device_name.append(str(self.cleanstr(device['ID_MODEL_ENC'])))
                
                if len(device_name) == 0 and 'ID_V4L_PRODUCT' in device:
                    device_name.append(self.cleanstr(device['ID_V4L_PRODUCT']))
                
                if 'ID_FS_LABEL' in device and 'ID_FS_TYPE' in device:
                    print("Have label")
                    device_name.append('%s (%s)' % (device['ID_FS_LABEL'], device['ID_FS_TYPE']))
                
                if len(device_name) > 0:
                    print("Have label",device_name)
                    # device_name.insert(0, result['type'])
                    result['name'] = "\n".join(str(dev) for dev in device_name)
                    print("Have label",result['name'])
                else:
                    result['name'] = result['type']
                return result
            else:
                continue
    def run(self):
        context = Context()
        monitor = Monitor.from_netlink(context)
        # monitor.filter_by(None)
        monitor.start()
        for device in iter(monitor.poll, None):
            detected = self.detect_device(device)
            send_notify(detected) 

def send_notify(device):
    if device:
        if device['action'] == 'add':
            Title = _('Device Added')
        elif device['action'] == 'remove':
            Title = _('Device Removed')
        elif device['action'] == 'change':
            Title = _('Device Changed')
        n = notify2.Notification(
                Title,
                device['name'],
                str(device['icon'] )  # Icon name
                )
        n.set_hint('append','volume')
        n.show()
if __name__ == "__main__":
    daemon = NotifyDaemon('/tmp/daemon-example.pid')
    notify2.init ("Python UDEV Notify")
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            sys.stderr.write("Unknown command")
            sys.exit(2)
            sys.exit(0)
    else:
        sys.stderr.write("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
